import {ResourceDao} from "../common_daos/ResourceDao";

export abstract class BaseResourceRepository {
    abstract getAll(query: any): Promise<ResourceDao[]>

    abstract insert(data: ResourceDao): void

    abstract update(id: any, data: ResourceDao): void

    abstract delete(id: number): void

    abstract getOne(id: any): Promise<ResourceDao | null>;

}