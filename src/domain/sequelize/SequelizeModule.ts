import SequelizeConnection from "./SequelizeConnection";
import SequelizeResourceRepository from "./repositories/SequelizeResourceRepository";
import CoreContainer from "../../common/ioc/CoreContainer";
import {BaseDbConnection} from "../../common/database/BaseDbConnection";
import {BaseResourceRepository} from "../repositories/BaseResourceRepository";

module.exports = function bind() {
    CoreContainer.bind(BaseDbConnection, SequelizeConnection);
    CoreContainer.bind(BaseResourceRepository, SequelizeResourceRepository);
};