import {SequelizeResourceModel} from "../daos/SequelizeResourceModel";
import {BaseResourceRepository} from "../../repositories/BaseResourceRepository";
import {injectable} from "../../../common/ioc/decorators";
import {ResourceDao} from "../../common_daos/ResourceDao";

@injectable
export default class SequelizeResourceRepository implements BaseResourceRepository {


    delete(id: number): void {
        try {
            SequelizeResourceModel.destroy({
                where: {
                    id
                }
            });
        } catch (error) {
            return error;
        }

    }

    async getAll(query: any): Promise<ResourceDao[]> {
        try {
            return await SequelizeResourceModel.findAll();
        } catch (error) {
            return error;
        }
    }

    async getOne(id: number): Promise<ResourceDao | null> {
        try {
            return await SequelizeResourceModel.findByPk(id);
        } catch (error) {
            throw error;
        }
    }

    insert(data: ResourceDao): void {
        try {
            SequelizeResourceModel.create(data);
        } catch (error) {
            throw error;
        }
    }

    update(id: any, data: ResourceDao): void {
        try {
            SequelizeResourceModel.update(
                {id: data.id, type: data.type, url: data.url},
                {returning: true, where: {id}}
            );
        } catch (error) {
            throw error;
        }

    }
}
