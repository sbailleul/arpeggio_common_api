import Sequelize, {Model} from "sequelize";
import {ResourceType} from "../../ResourceType";

export class SequelizeResourceModel extends Model{
    public id!: number;
    public type!: ResourceType;
    public url!: string;

    /**
     * Model have to be initialized by Model class static method 'init', method custom init permit to group initialization properties
     * @param sequelize
     */
    static customInit(sequelize: any){
        SequelizeResourceModel.init({
                id: {type: Sequelize.INTEGER, primaryKey: true},
                type: Sequelize.ENUM({values: Object.keys(ResourceType)}),
                url: Sequelize.STRING
            }, {sequelize, tableName: 'resource', timestamps: false}
        );
    }
}

