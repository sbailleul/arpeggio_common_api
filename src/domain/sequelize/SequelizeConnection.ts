import {BaseDbConnection} from "../../common/database/BaseDbConnection";
import DbConnector from "../../common/database/DbConnector";
import {Sequelize} from 'sequelize';
import {injectable} from "../../common/ioc/decorators";
import {SequelizeResourceModel} from "./daos/SequelizeResourceModel";

@injectable
export default class SequelizeConnection implements BaseDbConnection {

    private readonly _modelArray = [
        (sequelize: any) => SequelizeResourceModel.customInit(sequelize)
    ];

    private _sequelize: Sequelize | undefined;

    connect(dbConnector: DbConnector): boolean {

        const connectionConfig: any = dbConnector.connectionConfig;

        this._sequelize = new Sequelize(connectionConfig.db, connectionConfig.user, connectionConfig.password, {
            host: connectionConfig.host,
            dialect: connectionConfig.dialect
        });
        this.initModels();
        return true;
    }

    private initModels(){
        this._modelArray.forEach(customInit => customInit(this._sequelize));
    }
}
