import {ResourceType} from "../ResourceType";

export class ResourceDao{
    id: number;
    url: string;
    type: ResourceType;

    /**
     * All properties has to be initialize by object constructor like this one
     * @param obj
     */
    constructor(obj : {id: number, url: string, type: ResourceType}) {
        this.id = obj?.id;
        this.url = obj?.url;
        this.type = obj?.type;
    }
}