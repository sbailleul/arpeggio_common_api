import {Document, Model, Schema} from "mongoose";

export interface ISchema {

    createSchema(): Schema;

    getModel():Model<Document>;

}
