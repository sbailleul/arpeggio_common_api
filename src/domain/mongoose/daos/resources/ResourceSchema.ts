import {Document, model, Model, Schema} from "mongoose";
import {ISchema} from "../ISchema";
import {ResourceDoc} from "./ResourceDoc";


export default class ResourceSchema implements ISchema {

    private static _model: Model<Document>;

    createSchema(): Schema {
        return new Schema({
            id: {type: Number, unique: true},
            url: {type: String, required: true},
            type: {type: String, required: true}
        }, {versionKey: false});
    }

    getModel(): Model<Document> {
        if (!ResourceSchema._model) {
            ResourceSchema._model = this.createModel();
        }
        return ResourceSchema._model;
    }

    private createModel(): Model<Document> {
        const schema = this.createSchema();
        return model<ResourceDoc>('resource', schema);
    }

}

