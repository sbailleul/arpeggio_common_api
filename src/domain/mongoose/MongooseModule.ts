import MongooseResourceRepository from "./repositories/MongooseResourceRepository";
import MongooseConnection from "./MongooseConnection";
import CoreContainer from "../../common/ioc/CoreContainer";
import {BaseDbConnection} from "../../common/database/BaseDbConnection";
import {BaseResourceRepository} from "../repositories/BaseResourceRepository";

module.exports = function bind() {
    CoreContainer.bind(BaseDbConnection, MongooseConnection);
    CoreContainer.bind(BaseResourceRepository, MongooseResourceRepository);
};