import {Document, Model} from "mongoose";
import {ISchema} from "./daos/ISchema";
import {ResourceDao} from "../common_daos/ResourceDao";

export default class MongooseRepository {

    protected _model: Model<Document>;
    protected _mongoose = require('mongoose');

    constructor(schema: ISchema) {
        this._model = schema.getModel();
    }

    delete(id: number): void {
        if (!this._model) {
            return;
        }

        this._model.findByIdAndDelete(id);

    }

    async getOne(id: any): Promise<any> {
        if (!this._model || !this._mongoose.isValidObjectId(id)) {
            return;
        }

        return this._model.findById(id);
    }

    async getAll(query: any): Promise<any> {
        if (!this._model) {
            return;
        }
        let {skip, limit} = query;

        skip = skip ? Number(skip) : 0;
        limit = limit ? Number(limit) : 10;

        delete query.skip;
        delete query.limit;

        return this._model
            .find(query)
            .skip(skip)
            .limit(limit);
    }

    insert(data: ResourceDao): void {
        if (!this._model) {
            return;
        }
        this._model.create(data);
    }

    update(id: any, data: ResourceDao): void {
        if (!this._model) {
            return;
        }
        this._model.findByIdAndUpdate(id, data, {new: true});

    }
}
