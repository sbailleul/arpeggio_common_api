import {BaseDbConnection} from "../../common/database/BaseDbConnection";
import DbConnector from "../../common/database/DbConnector";
import {injectable} from "../../common/ioc/decorators";

// @injectable(InjectableTypes.IDbConnection)
@injectable
export default class MongooseConnection implements BaseDbConnection {

    private _mongoose: any;

    constructor() {
        this._mongoose = require('mongoose');
    }

    connect(dbConnector: DbConnector): boolean {
        const connectionConfig = dbConnector.connectionConfig;
        try {
            this._mongoose.connect(this.createConnectionString(dbConnector), {
                authSource: "admin",
                useNewUrlParser: true,
                user: connectionConfig.user,
                pass: connectionConfig.password,
                useUnifiedTopology: true,
                useCreateIndex: true
            });
        } catch (err) {
            throw err;
        }
        return true;
    }

    createConnectionString(dbConnector: DbConnector): string {

        const connectionString = new URL("mongodb://");
        const connectionConfig: any = dbConnector.connectionConfig;
        connectionString.host = connectionConfig.host;
        connectionString.pathname = connectionConfig.db;
        connectionString.port = connectionConfig.port;
        const test = connectionString.toString();
        return test
    }
}


