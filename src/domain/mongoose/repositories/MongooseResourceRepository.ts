import MongooseRepository from "../MongooseRepository";
import ResourceSchema from "../daos/resources/ResourceSchema";
import {BaseResourceRepository} from "../../repositories/BaseResourceRepository";
import {injectable} from "../../../common/ioc/decorators";

@injectable
export default class MongooseResourceRepository extends MongooseRepository implements BaseResourceRepository {

    constructor() {
        super(new ResourceSchema());
    }
}
