// Have to be import first for dependency injection
import ModuleBinder from "./common/ioc/ModuleBinder";
ModuleBinder.bindModules();

import DbConnectorFactory from "./common/database/DbConnectorFactory";
import ServerFactory from "./common/server/ServerFactory";
import ResourcesConfig from "./common/resources/ResourcesConfig";

const dbConnector = DbConnectorFactory.getInstance(ResourcesConfig.GLOBAL_CONFIG.dbConnectorType).getDbConnector();
const serverConnector = ServerFactory.getInstance(ResourcesConfig.GLOBAL_CONFIG.serverType).getServerConnector();

if (dbConnector && serverConnector) {
    serverConnector.setRoutes();
    dbConnector.connect();
    serverConnector.listen();
}



