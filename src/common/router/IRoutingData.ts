import {RequestHandler} from "express";

/**
 * Represent required information to route http request to controllers
 */
export interface IRoutingData{
    url: string;
    httpMethod: string;
    middlewares: RequestHandler[];
}