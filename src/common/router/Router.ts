import {BaseServer} from "../server/BaseServer";
import ResourceController from "../../web/controllers/ResourceController";

export default class Router {

    private static readonly _controllers = [
        new ResourceController()
    ];

    public static setRoutes(server: BaseServer) {
        this._controllers.forEach(c => c.routingData.forEach(r => server.route(r)));
    }

    public static changeBasePath(url: string, newBase: string): string {
        const path = url.split("/");

        if (url[0] === '/') {
            path[1] = newBase;
        } else {
            path[0] = newBase;
        }

        const filteredPath = path.filter(substring => substring.trim() !== '');
        return filteredPath.join("/");
    }
}
