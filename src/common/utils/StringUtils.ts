export class StringUtils{

    static strictEqual(el1: string, el2: string ): boolean{
        return el1 === el2;
    }

    static laxEqual(el1: string, el2: string ): boolean{
        return el1.indexOf(el2) > -1 ;
    }

    static getLastElementByDelimiter(target: string, delimiter: string ): string{
        const elements = target.split(delimiter);
        return elements[elements.length - 1];
    }
}