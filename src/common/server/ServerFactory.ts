import ServerConnector from "./ServerConnector";
import {ServerType} from "./ServerType";
import ResourcesConfig from "../resources/ResourcesConfig";

export default class ServerFactory {

    private static instance: ServerFactory;
    private _serverConnector: ServerConnector;
    private readonly serverType: string;

    constructor(serverType: string) {
        this.serverType = serverType;
    }

    public static getInstance(serverType?: string): ServerFactory {

        if (!this.instance && serverType) {
            this.instance = new ServerFactory(serverType);
        }
        return this.instance;
    }

    public  getServerConnector(): ServerConnector {

        switch (this.serverType) {
            case ServerType.EXPRESS:
                this._serverConnector = new ServerConnector(ResourcesConfig.EXPRESS_CONFIG);
                break;
        }

        return this._serverConnector;
    }
}
