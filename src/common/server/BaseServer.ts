import ServerConnector from "./ServerConnector";
import {IRoutingData} from "../router/IRoutingData";

export abstract class BaseServer {

    abstract listen(serverConnector: ServerConnector): void;

    abstract route(route: IRoutingData): void;
}
