import {BaseServer} from "./BaseServer";
import Router from "../router/Router";
import {injectedProperty, injectionTarget} from "../ioc/decorators";

@injectionTarget
export default class ServerConnector {

    private readonly _port: number;
    @injectedProperty(BaseServer)
    private readonly _server: BaseServer;


    constructor(data: { port: number }) {
        this._port = data.port;
    }


    get port(): number {
        return this._port;
    }

    listen(){
        this._server.listen(this);
    }

    setRoutes(){
        Router.setRoutes(this._server);
    }
}
