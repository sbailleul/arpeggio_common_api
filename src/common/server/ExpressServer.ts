import {BaseServer} from "./BaseServer";
import express, {Express} from "express";
import ServerConnector from "./ServerConnector";
import bodyParser from "body-parser";
import ObjectUtil from "../utils/ObjectUtil";
import {injectable} from "../ioc/decorators";
import {IRoutingData} from "../router/IRoutingData";

// @injectable(InjectableTypes.IServer)
@injectable
export default class ExpressServer implements BaseServer {

    private readonly server: Express;

    constructor() {
        this.server = express();
        this.server.use(bodyParser.json());
    }

    listen(serverConnector: ServerConnector): void {
        try {
            this.server.listen(serverConnector.port, () => {
                console.log(`app running on port ${serverConnector.port}`);
            })
        } catch (err) {
            throw err;
        }
    }

    route(routingData: IRoutingData): void {

        if (!ObjectUtil.isComplete(routingData)) {
            return
        }
        const httpMethod = routingData.httpMethod.toLowerCase() as any as unknown;
        const func = ObjectUtil.getObjectValueByKey(this.server, httpMethod);
        func.call(this.server, routingData.url, routingData.middlewares);
        console.log(httpMethod);
    }


}
