import DbConnector from "./DbConnector";

/**
 * Db connection to communicate with databases SQL or MongoDB for now
 */
export abstract class BaseDbConnection {

    abstract connect(dbConnector: DbConnector): boolean;
}
