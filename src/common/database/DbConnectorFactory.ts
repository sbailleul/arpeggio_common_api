import DbConnector from "./DbConnector";
import ResourcesConfig from "../resources/ResourcesConfig";
import {DbConnectionType} from "./DbConnectionType";

export default class DbConnectorFactory {

    private static _instance: DbConnectorFactory;
    private _dbConnector: DbConnector ;
    private readonly _dbConnectionType: string;

    constructor(dbConnectorType: string) {
        this._dbConnectionType = dbConnectorType;
    }


    /**
     * Return instance of DbConnectorFactory, init _dbConnector with _dbConnectorType if _instance is undefined
     * @param dbConnectorType? DbConnectionType used to choose database configuration for DbConnector constructor
     */
    public static getInstance(dbConnectorType?: string): DbConnectorFactory {

        if (!this._instance && dbConnectorType) {
            this._instance = new DbConnectorFactory(dbConnectorType);
        }
        return this._instance;
    }

    /**
     * Return instance of _dbConnector, create it with database configuration if not exist yet
     */
    public getDbConnector(): DbConnector {

        if(!this._dbConnector){
            switch (this._dbConnectionType) {
                case DbConnectionType.SEQUELIZE:
                    this._dbConnector = new DbConnector(ResourcesConfig.SEQUELIZE_CONFIG);
                    break;
                case DbConnectionType.MONGOOSE:
                    this._dbConnector = new DbConnector(ResourcesConfig.MONGOOSE_CONFIG);
                    break;
            }
        }
        return this._dbConnector;
    }
}
