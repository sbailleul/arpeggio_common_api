/**
 * Enum to list available dbConnection in application
 */
export enum DbConnectionType {
    SEQUELIZE = 'SEQUELIZE',
    MONGOOSE = 'MONGOOSE'
}
