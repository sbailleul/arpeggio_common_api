import {BaseDbConnection} from "./BaseDbConnection";
import {injectedProperty, injectionTarget} from "../ioc/decorators";

// Use to connect application to database
@injectionTarget
export default class DbConnector {


    private readonly _connectionConfig: any;

    @injectedProperty(BaseDbConnection)
    private readonly _dbConnection: BaseDbConnection;

    constructor(connectionConfig: any) {
        this._connectionConfig = connectionConfig;
    }

    get connectionConfig() {
        return this._connectionConfig;
    }


    get dbConnection(): BaseDbConnection {
        return this._dbConnection;
    }

    /**
     * Connect to database by _dbConnection
     */
    connect(){
        try{
            this.dbConnection.connect(this);
        } catch(err){
            throw err;
        }
    }

}
