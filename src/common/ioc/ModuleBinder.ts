/**
 * Class to add binding for dependency injection.
 */
import ResourcesConfig from "../resources/ResourcesConfig";
import {DbConnectionType} from "../database/DbConnectionType";
/**
 * BIND ORDER :
 * 1. domain = Repositories & common_daos validators
 * 2. common = Web server
 * 3. core = Services
 */
export default class ModuleBinder {


    public static bindModules() {
        if (ResourcesConfig.GLOBAL_CONFIG.dbConnectorType === DbConnectionType.SEQUELIZE) {
            this.requireAndBind("../../domain/sequelize/SequelizeModule");
        } else if (ResourcesConfig.GLOBAL_CONFIG.dbConnectorType === DbConnectionType.MONGOOSE) {
            this.requireAndBind("../../domain/mongoose/MongooseModule");
        }
        this.requireAndBind("../CommonModule");
        this.requireAndBind("../../core/CoreModule");
    }

    private static requireAndBind(path: string) {
        const bind = require(path);
        bind();
    }
}