import "reflect-metadata";
import {InjectedProperty} from "./InjectedProperty";
import {ConstructorType} from "./ConstructorType";
import {InjectedParameter} from "./InjectedParameter";

export default class CoreContainer {

    private static _typedProviders = new Map<ConstructorType<object>, ConstructorType<object>>();
    private static _injectables: ConstructorType<object>[] = [];
    private static _injectionTargets = new Map<string, { properties: InjectedProperty[], ctrParam: InjectedParameter[] }>();

    /**
     * Bind new provider in _typedProvides Map
     * @param type Custom type used to retrieve constructors in _typedProvides
     * @param provider Class constructor call during injection
     */
    public static bind(type: ConstructorType<any> | ReturnType<any>, provider: ConstructorType<object>) {
        if (!this.isInjectable(provider))
            return;
        if (this._typedProviders.has(type)) {
            console.warn(`Type ${type.name} already mapped`);
        }
        this._typedProviders.set(type, provider);
    }


    /**
     * Add injectable constructor to check during binding if class to bind is injectable
     * @param classPrototype Constructor to stock in _injectables
     */
    public static addInjectable(classPrototype: ConstructorType<object>) {
        this._injectables.push(classPrototype);
    }

    /**
     * Reference class properties to inject later in injectProperties
     * @param destClass Destination class for injection
     * @param property Target property for injection
     */
    public static addInjectedProperty(destClass: ConstructorType<object>, property: InjectedProperty) {

        const className = this.initClassInjectionTypes(destClass);
        this._injectionTargets.get(className)!.properties.push(property);
    }


    /**
     * Inject properties to class instance, look in _injectionTargets if class is referenced, if it's true try to find corresponding constructor to call to init property.
     * @param proto Class prototype
     */
    public static injectProperties(proto: object) {

        const className = proto.constructor.name;
        const propertiesToInject = this._injectionTargets.get(className)?.properties;
        if (!propertiesToInject) {
            console.warn(`No properties to inject for ${className}`);
        } else {
            propertiesToInject.forEach(property => {
                const provider = this._typedProviders.get(property.type);
                if (provider) {
                    Reflect.set(proto, property.propertyKey, new provider());
                } else {
                    console.warn(`No provider of type ${property.type.name} found for property ${property.propertyKey}`);
                }
            })
        }
    }

    /**
     * Check if destClass is in _injectionTargets and add it if not
     * @param destClass Check if class is in _injectionTargets if not add it
     * @return Class name from destClass
     */
    private static initClassInjectionTypes(destClass: ConstructorType<object>): string {
        const className = destClass.name;
        if (!this._injectionTargets.has(className)) {
            this._injectionTargets.set(className, {properties: [], ctrParam: []});
        }
        return className;
    }

    /**
     * Test if class constructor exist in _injectables
     * @param provider Class to verify
     */
    private static isInjectable(provider: ConstructorType<object>): boolean {
        return this._injectables.find(i => i.name === provider.name) !== undefined;
    }

}
