import CoreContainer from "./CoreContainer";
import {ConstructorType} from "./ConstructorType";

/**
 * Class decorator trigerred when class is referenced for example during import
 * @param constructor Class constructor targeted by injection, return constructor with properties to inject set
 */
export function injectionTarget<T>(constructor: ConstructorType<T>) {
    CoreContainer.injectProperties(constructor.prototype);
    return constructor;
}

/**
 * Property decorator trigerred when class is referenced for example during import
 * @param type
 */
// Specify class properties are injected by ioc
export function injectedProperty(type: ConstructorType<object> | ReturnType<any>): any {

    return function (target: any, propertyKey: string) {
        CoreContainer.addInjectedProperty(target.constructor, {propertyKey, type});
    }

}

// Specify than type are injectable by ioc
export function injectable(constructor: ConstructorType<object> | ReturnType<any>) {
    CoreContainer.addInjectable(constructor);
}
