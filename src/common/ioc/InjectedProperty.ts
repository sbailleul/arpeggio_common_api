import {ConstructorType} from "./ConstructorType";

/**
 *  Contain name and type of an injected property
 */
export interface InjectedProperty {
    propertyKey: string
    type: ConstructorType<object>
}

