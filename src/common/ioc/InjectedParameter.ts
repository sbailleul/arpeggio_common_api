import {InjectableTypes} from "./InjectableTypes";

export interface InjectedParameter {
    parameterIndex: number;
    propertyKey: string;
    type: InjectableTypes
}