// List injectable types for ioc
export enum InjectableTypes {
    ResourceValidator = 'ResourceValidator',
    IDbConnection = 'IDbConnection',
    IServer = 'IServer',
    IResourceRepository = 'IResourceRepository',
    IResourceService = 'IResourceService'
}
