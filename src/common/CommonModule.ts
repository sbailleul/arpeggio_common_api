import ExpressServer from "./server/ExpressServer";
import CoreContainer from "./ioc/CoreContainer";
import {BaseServer} from "./server/BaseServer";

module.exports = function bind() {
    CoreContainer.bind(BaseServer, ExpressServer);
};