export default class ModuleUtils {

    public static async getDefaultModuleAsync(path: string): Promise<any> {
        const module = await import(path);
        return module.default;
    }
}