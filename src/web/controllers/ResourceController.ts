import {Controller} from "./Controller";
import {BaseResourceService} from "../../core/services/BaseResourceService";
import {injectedProperty, injectionTarget} from "../../common/ioc/decorators";
import ResourceCreateDto from "../../core/dtos/ResourceFriendlyDto";
import {Validator} from "../../core/Validator";


@injectionTarget
export default class ResourceController extends Controller {

    @injectedProperty(BaseResourceService)
    protected _service: BaseResourceService;

    constructor() {
        super();
        this.initRoutingData();
        this.changeBaseUrls("/resources");
        this.addMiddleWareToEndPointByMethodNameComparison(0, new Validator(ResourceCreateDto).validate, this.insert);
    }

}
