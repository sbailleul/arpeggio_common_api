import {NextFunction, Request, Response, RequestHandler} from "express";
import {IRoutingData} from "../../common/router/IRoutingData";
import Router from "../../common/router/Router";
import {BaseService} from "../../core/services/BaseService";
import {StringUtils} from "../../common/utils/StringUtils";

export abstract class Controller {

    protected abstract _service: BaseService;
    protected _httpConstants = require('http2').constants;
    protected _routingData: IRoutingData[];

    constructor() {
        this.getOne = this.getOne.bind(this);
        this.getAll = this.getAll.bind(this);
        this.insert = this.insert.bind(this);
        this.update = this.update.bind(this);
        this.delete = this.delete.bind(this);
    }


    get routingData(): IRoutingData[] {
        return this._routingData;
    }

    async getOne(req: Request, res: Response, next: NextFunction): Promise<any> {
        const {id} = req.params;
        try {
            const entity = await this._service.getOne(id);

            if (!entity) {
                return res.status(this._httpConstants.HTTP_STATUS_NOT_FOUND).send();
            }
            return res.status(this._httpConstants.HTTP_STATUS_OK).send(entity);
        } catch (error) {
            return res.status(this._httpConstants.HTTP_STATUS_INTERNAL_SERVER_ERROR).send(error);
        }
    }

    async getAll(req: Request, res: Response, next: NextFunction): Promise<any> {
        const entities = await this._service.getAll(req.query);
        return res.status(this._httpConstants.HTTP_STATUS_OK).send(entities);
    }

    async insert(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            await this._service.insert(req.body);
            return res.status(this._httpConstants.HTTP_STATUS_CREATED).send();
        } catch (error) {
            return res.status(this._httpConstants.HTTP_STATUS_INTERNAL_SERVER_ERROR).send(error);
        }
    }

    async update(req: Request, res: Response, next: NextFunction): Promise<any> {
        const {id} = req.params;

        try {
            const entity = await this._service.update(id, req.body);
            return res.status(this._httpConstants.HTTP_STATUS_NO_CONTENT).send(entity);
        } catch (error) {
            return res.status(this._httpConstants.HTTP_STATUS_INTERNAL_SERVER_ERROR).send(error);
        }

    }

    async delete(req: Request, res: Response, next: NextFunction): Promise<any> {
        const {id} = req.params;
        try {
            const entity = await this._service.delete(id);
            return res.status(this._httpConstants.HTTP_STATUS_NO_CONTENT).send(entity);
        } catch (error) {
            return res.status(this._httpConstants.HTTP_STATUS_INTERNAL_SERVER_ERROR).send(error);
        }
    }

    protected changeBaseUrls(newBase: string) {
        this.routingData.forEach(route => route.url = Router.changeBasePath(route.url, newBase));
    }

    protected addMiddleWareToEndPointByMethodNameComparison(pos: number, middleware: RequestHandler, endPoint: any, isStrictComparison: boolean = false){
        this._routingData.forEach(r => {
            r.middlewares.forEach(e => {
                let areEqual: boolean;
                if(isStrictComparison){
                    areEqual = StringUtils.strictEqual(e.name, endPoint.name);
                } else {
                    areEqual = StringUtils.laxEqual(e.name, StringUtils.getLastElementByDelimiter(endPoint.name, " "));
                }
                if(areEqual)
                    r.middlewares.splice(pos, 0, middleware);
            })
        })
    }

    protected initRoutingData() {

        this._routingData = [
            {url: '/', httpMethod: this._httpConstants.HTTP2_METHOD_GET, middlewares: [this.getAll]},
            {
                url: '/',
                httpMethod: this._httpConstants.HTTP2_METHOD_POST,
                middlewares: [this.insert]
            },
            {url: '/' + '/:id', httpMethod: this._httpConstants.HTTP2_METHOD_GET, middlewares: [this.getOne]},
            {
                url: '/' + '/:id',
                httpMethod: this._httpConstants.HTTP2_METHOD_PUT,
                middlewares: [this.update]
            },
            {url: '/' + '/:id', httpMethod: this._httpConstants.HTTP2_METHOD_DELETE, middlewares: [this.delete]}
        ];
    }

}
