import ResourceService from "./services/ResourceService";
import CoreContainer from "../common/ioc/CoreContainer";
import {BaseResourceService} from "./services/BaseResourceService";

module.exports = function bind() {
    CoreContainer.bind(BaseResourceService, ResourceService);
};
