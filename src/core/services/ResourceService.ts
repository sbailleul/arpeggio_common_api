import {BaseResourceService} from "./BaseResourceService";
import {injectable, injectedProperty, injectionTarget} from "../../common/ioc/decorators";
import {BaseResourceRepository} from "../../domain/repositories/BaseResourceRepository";

@injectable
@injectionTarget
export default class ResourceService implements BaseResourceService {


    @injectedProperty(BaseResourceRepository)
    protected readonly _repository: BaseResourceRepository;

    constructor() {
        this.getAll = this.getAll.bind(this);
        this.getOne = this.getOne.bind(this);
        this.insert = this.insert.bind(this);
        this.update = this.update.bind(this);
        this.delete = this.delete.bind(this);
    }

    async getOne(id: any): Promise<any> {
        return this._repository.getOne(id);
    }

    async getAll(query: any): Promise<any> {
        return this._repository.getAll(query);
    }

    insert(data: any): void {
        try {
            this._repository.insert(data);
        } catch (error) {
            throw error;
        }
    }

    update(id: any, data: any): void {
        try {
            this._repository.update(id, data);

        } catch (error) {
            throw error;
        }
    }

    delete(id: any): void {
        try {
            this._repository.delete(id);

        } catch (error) {
            throw error;
        }
    }
}

