export abstract class BaseService {
    abstract getOne(id: any): Promise<any>

    abstract getAll(query: any): Promise<any>

    abstract insert(data: any): void

    abstract update(id: any, data: any): void

    abstract delete(id: any): void
}