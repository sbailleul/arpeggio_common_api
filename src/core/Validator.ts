import {ConstructorType} from "../common/ioc/ConstructorType";
import {NextFunction, Request, Response} from "express";
import {validate} from "class-validator";
import ResourceFriendlyDto from "./dtos/ResourceFriendlyDto";

export class Validator<T>{
    private readonly _dtoCtr: ConstructorType<T>;
    private _httpConstants = require('http2').constants;

    constructor(dtoCtr: ConstructorType<T>) {
        this._dtoCtr = dtoCtr;
        this.validate = this.validate.bind(this);
    }

    public async validate(req: Request, res: Response, next: NextFunction): Promise<any>{
        const test = new ResourceFriendlyDto(req.body);
        const resource = new this._dtoCtr(req.body);
        const errors = await validate(resource);
        if(errors.length > 0){
            return res.status(this._httpConstants.HTTP_STATUS_UNPROCESSABLE_ENTITY).send(errors);
        }
        next();
    }
}