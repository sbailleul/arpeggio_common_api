import {IsNotEmpty, Length} from "class-validator";

export default class ResourceFriendlyDto {

    @IsNotEmpty()
    id: number;

    @Length(0, 46)
    url: string;

    /**
     *
     * @param obj
     */
    constructor(obj: { id: number, url: string }) {
        this.id = obj?.id;
        this.url = obj?.url;
    }
}