import ResourceFriendlyDto from "../dtos/ResourceFriendlyDto";
import {Mapper} from "./Mapper";
import {ConstructorType} from "../../common/ioc/ConstructorType";
import {ResourceDao} from "../../domain/common_daos/ResourceDao";

export default class ResourceMapper extends Mapper{

    constructor(){
        super();
        this._automapper.createMap(ResourceFriendlyDto.name, ResourceDao.name).convertToType(ResourceDao);
        this._automapper.createMap(ResourceDao.name, ResourceFriendlyDto.name).convertToType(ResourceFriendlyDto);
        this.addMapping(ResourceFriendlyDto, ResourceDao);
    }
    strictMap<S>(src: S, dst:ConstructorType<any> ): any{
        return this.map(src, dst);
    }

    /**
     * Get on object and return corresponding mapped object if mapping is registered by mapper
     * @param src
     * @param dst
     */
    map(src: any, dst:ConstructorType<any> ): any{
        const srcCtr = Object.getPrototypeOf(src)?.constructor;
        if(!this._map.has(srcCtr)){
            console.warn(`No mapping registered for type ${srcCtr.name}`)
            return;
        }
        if(!this._map.get(srcCtr)?.find(ctr => ctr.name === dst.name)){
            console.warn(`No class mapping of type ${ dst.name} for type ${srcCtr.name}`)
            return;
        }
        return this._automapper.map(srcCtr.name, dst.name, src);
    }
}
