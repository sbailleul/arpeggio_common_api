import {ConstructorType} from "../../common/ioc/ConstructorType";
import {MapUtil} from "../../common/utils/MapUtil";
// @ts-ignore
import automapper from "automapper-ts";

export abstract class Mapper{

    protected _automapper = automapper;
    protected _map = new Map<ConstructorType<any>, ConstructorType<any>[]>();

    protected addMapping(src: ConstructorType<any>, dst: ConstructorType<any>){
        MapUtil.addElementToArrayMap(this._map, src, dst);
    }

}